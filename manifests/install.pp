# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include qtractor::install
class qtractor::install {
  package { $qtractor::package_name:
    ensure => $qtractor::package_ensure,
  }
}
