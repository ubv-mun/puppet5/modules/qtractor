# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include qtractor
class qtractor (
  String        $package_ensure,
  Array[String] $package_name,
) {
  contain 'qtractor::install'
}
